#!/bin/bash
# Copyright (C) 2017 Codethink Limited
# Author: Tristan Van Berkom <tristan@codethink.co.uk>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library. If not, see <http://www.gnu.org/licenses/>.


# Begin exclusive locking, this will abort the build for --interval
# invocations or block for --schedule invocations
exec 201> @@TOPDIR@@/cargo_lock
if ! flock 201  ; then
    exit 1
fi

timestamp=$(date +%Y-%m-%d-%H%M%S)
logdir=@@EXPORTDIR@@/cargo-fetch-logs
logfile=${logdir}/cargo-fetch-${timestamp}.txt

mkdir -p "${logdir}"

# Possibly we hand installed ostree into /usr/local prefix
# and also possibly we have rusty things installed in ~/.cargo
export PATH=${HOME}/.cargo/bin:/usr/local/bin:$PATH

echo "====================================================" >> $logfile
echo "Starting cargo fetch job at `date`" >> $logfile
echo "====================================================" >> $logfile

# Just manually time it in seconds
#
# We use a low priority for the build process to ensure that
# apache's serving of logs and build results has priority over the builds.
starttime=$(date +%s)
nice -n10 ionice -c2  @@TOPDIR@@/vendor.sh \
     --date "${timestamp}" \
     --gpg @@GPGKEY@@ --gpg-home @@GPGHOME@@ \
     --ostree @@EXPORTDIR@@/crates >> $logfile 2>&1
error_code=$?
endtime=$(date +%s)

seconds=$((endtime-starttime))
duration=$((seconds/86400))" days "$(date -d "1970-01-01 + $seconds seconds" "+%H hours %M minutes %S seconds")

echo "====================================================" >> $logfile
if [ "${error_code}" -eq "0" ]; then
    echo "Completed successfully in: $duration" >> $logfile
    echo "====================================================" >> $logfile
else
    echo "Failed in: $duration" >> $logfile
    echo "====================================================" >> $logfile
fi


# End exclusive lock
exec 201>&-
